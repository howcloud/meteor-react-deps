/*

// Example component

var Component = React.createClass({
	
	// ReactMeteor mixin

	mixins: [ReactMeteor],

	// React Meteor State defined as statics

	statics: {
		meteorPropDeps: [], // array (or null if none) of the properties which should force us to update subscriptions on the client side
							// we still subscribe within a Deps.autorun to react to usage of Session vars etc
		meteorStateDeps: [], // see above but for state, careful not to make in infinite loop here but does allow us to make meteorState dependent on other state values

		meteorSubscribe: function (props, state) { // client side subscription management for given prop set, should return all subscriptions as an array
			return [Meteor.subscribe('Names_allNames')] // for example
		},

		getMeteorState: function (props, state) { // client and server side function to get current meteor state
										   		  // can separate client and server side by implement isServer and isClient conditions here

			return {
				names: Names.find().fetch(),
			}
		},
	},
	
	// Normal component definition continues from here

});

*/

//readyComputations = []; // used for global debugging

var ReactMeteorMixin = {

	/** Meteor **/

	/* Subscriptions */

	_meteorSubscribe_computation: null, // dependency computation for current meteorSubscribe
	_meteorSubscribe_readyComputation: null, // dependency computation for whether the current meteor subscription set is ready/has been fetched

	_meteorSubscribe: function (hideLoading) {
		//console.log(this.constructor);
		if (!this.constructor.meteorSubscribe) return this._meteorSubscribe_ready();

		var computation = Deps.autorun(function () { // the whole thing is a dependency as if the things that meteorSubscribe depends on change (ie session info) then we need to start a new ready computation etc too
			// End state computation
			// We don't want to refresh the state based on bogus values now that our dependencies have changed [this could possibly be moved out of this dependency. Perhaps....]
			// This has the effect of 'freezing' the state until we have our new subscription data - but is this the effect we want ??? 

			if (this._meteorState_computation) {
				this._meteorState_computation.stop();
				this._meteorState_computation = null;
			}

			// Subscription computation

			var subs = this.constructor.meteorSubscribe(this.props, this.state);
			//console.log(subs);
			if (!(subs instanceof Array)) subs = [subs];
			
			// Ready computation

			if (!hideLoading) this.setState({_meteorLoading: true}); // are we currently pulling down subscription data

			Deps.nonreactive(function () { // this is really important, in theory, but I don't really know why. Fixes bug when inserted!
				var ready = true;
				var readyComputation = Deps.autorun(function () { // runs once straight away initially, hence ready check being performed straight away to decide if we need to carry on with the computation
					ready = true; // have to set this again here so that we set to true again whenever this is rerun

					if (subs.length) _.each(subs, function (sub) { if (!sub.ready()) ready = false; });
					// console.log(ready);

					if (ready) {
						/*if (this.ISCOURSEVIEW) {
							console.log("ready");
							console.log(subs);
						}*/

						setTimeout(this._meteorSubscribe_ready, 0);
					}
				}.bind(this));

				if (ready) {
					readyComputation.stop();
				} else {
					if (this._meteorSubscribe_readyComputation) this._meteorSubscribe_readyComputation.stop(); // see above
					this._meteorSubscribe_readyComputation = readyComputation;

					/*readyComputation.onInvalidate(function () {
						console.log("invalidate "+_.indexOf(readyComputations, this._meteorSubscribe_readyComputation));
						console.log(new Error().stack);
					}.bind(this));*/

					//console.log(this.constructor);
					//readyComputations.push(readyComputation);
					//console.log("start "+(readyComputations.length-1));
				}	
			}.bind(this));		
		}.bind(this));

		if (this._meteorSubscribe_computation) this._meteorSubscribe_computation.stop(); // stop the old computation if we are forcing a new one to be initiated (ie after a dependent state or prop change)
																						 // we only stop the old computation when we have established the new computation [although not sure if this actually matters]
		this._meteorSubscribe_computation = computation;

	},

	_meteorSubscribe_ready: function () {
		//if (this.ISCOURSEVIEW) console.log("subscribe ready");
		// console.log("subscribe ready");

		Deps.nonreactive(function () {
			if (!this._react_mounted) return;
 
			// Previously we were setting state here, but setState calls are not necessarily batched by React so we don't want to have state where _meteorLoading: false and none of the expected meteor loaded data has arrived
			// this.setState({_meteorLoading: false, _meteorInitialLoad: false});		

			if (this._meteorSubscribe_readyComputation) {
				/*console.log(this.constructor);
				console.log("end "+_.indexOf(readyComputations, this._meteorSubscribe_readyComputation));*/

				this._meteorSubscribe_readyComputation.stop();
				this._meteorSubscribe_readyComputation = null;
			}

			if (!this.constructor.getMeteorState) return;

			/** State computation [updates whenever the data underlying the subscriptions etc changes] **/

			// flags are used to make sure we are only setting these state variables on this call rather than whenever an autorun is made, saves comparisons on each
			// used to communicate to this._meteorState_refresh to set _meteorLoading = false and _meteorInitialLoad = false

			this.unflagMeteorInitialLoad = true; 
			this.unflagMeteorLoad = true;
			var stateComputation = Deps.autorun(this._meteorState_refresh);
			this.unflagMeteorInitialLoad = false;
			this.unflagMeteorLoad = false;

			this._meteorState_computation = stateComputation; // this should be null as we stop the current state computation if there is one going when we start a new subscription
			
		}.bind(this));
	},

	/* State */

	_meteorState_computation: null, // holds the dependency computation for the current meteor state 

	_meteorState_get: function () {
		if (!this.constructor.getMeteorState) return {};
		
		return this.constructor.getMeteorState(this.props, this.state);
	},

	_meteorState_refresh: function () {
		if (!this._react_mounted) {
			/*if (this.ISCOURSEVIEW) {
				console.log("meteorState :: do not refresh");
				console.log(this.isMounted());
				console.log(this._compositeLifeCycleState);
			}*/

			return;
		}

		var state = this._meteorState_get();
		if (this.unflagMeteorInitialLoad) state._meteorInitialLoad = false;
		if (this.unflagMeteorLoad) state._meteorLoading = false;

		/*if (this.ISCOURSEVIEW) {
			console.log("meteorState :: refresh");
			console.log(state);
		}*/
		
		// console.log("got state");
		// console.log(state);

		Deps.nonreactive(function () {
			this.setState(state);
		}.bind(this));
	},

	/** React Lifecycle **/		

	_react_mounted: false, // whether we consider this mounted for the purposes of react
						   // we used to be able to check (prior to 0.13) using a combination of this.isMounted() || this._compositeLifeCycleState === "MOUNTING" but this doesn't work in 0.13.3
						   // see changed definition isMounted and fact that lifeCycleState is no longer defined anywhere

	componentWillMount: function() {
		this._react_mounted = true;
		if (!this.constructor.getMeteorState) return;

		if (Meteor.isClient) {
			Deps.nonreactive(function () { // happens because we are in a dep context when we render sometimes AH HAH!!!
				this.setState({_meteorInitialLoad: true}); // this is set to true only for the initial loading of data

				if (ReactMeteor.REACT_METEOR_INJECT_CHECK) {
					var checksum = meteorReactChecksum(this);

					var injectedState = Injected.obj('ims'+checksum);
					if (injectedState) {
						var injectState = {};
						_.each(injectedState, function (value, key) {
							injectedState[key] = _meteor_awakenState(value);
						});

						injectedState._meteorLoading = false; 
						injectedState._meteorInitialLoad = false;
						injectedState._meteorInitialInjected = true;

						this.setState(injectedState); // only do the subscribe once we have injected the state as subscribe can be reliant on state
													  // NOTE: A callback is not called for setState's before the initial render, instead we look for _meteorInitialInjected in componentDidMount (see below)
					} else {
						this._meteorSubscribe();
					}
				} else {
					this._meteorSubscribe();
				}
			}.bind(this));
		}

		if (Meteor.isServer) {
			var state = this._meteorState_get();
			this.setState({
				_meteorLoading: false, 
				_meteorInitialLoad: false, 
				_meteorInitialInjected: true
			}); // don't want to make part of state object else will be transported to client unecessarily
				// need to put into state, however, so that the rendered output is the same
			
			if (state) {
				this.setState(state);
				
				var checksum = meteorReactChecksum(this);
				Inject.obj('ims'+checksum, state, RequestVars.get().res);
			}
		}
	},

	componentDidMount: function () {
		Deps.nonreactive(function () {
			if (this.state._meteorInitialInjected) this._meteorSubscribe(true);
		}.bind(this));
	},

	componentWillUnmount: function () {
		this._react_mounted = false;

		if (this._meteorState_computation) this._meteorState_computation.stop();
		if (this._meteorSubscribe_computation) this._meteorSubscribe_computation.stop();
		if (this._meteorSubscribe_readyComputation) this._meteorSubscribe_readyComputation.stop();
	},

	componentWillUpdate: function (nextProps, nextState) {
		// Work out if we need to refresh our meteor state due to props/state changes
		var update = false;
		var currProps = this.props;
		var currState = this.state;

		if (this.constructor.meteorPropDeps) {
			_.every(this.constructor.meteorPropDeps, function (prop) {
				if (!_.isEqual(currProps[prop], nextProps[prop])) {
					update = true;
					return false;
				}

				return true;
			});
		}

		if (!update && this.constructor.meteorStateDeps) {
			_.every(this.constructor.meteorStateDeps, function (state) {
				if (!_.isEqual(currState[state], nextState[state])) {
					update = true;
					return false;
				}

				return true;
			});
		}

		if (update) setTimeout(this._meteorSubscribe, 0); // setTimeout as we can't setState in componentWillUpdate and yet we won't be able to catch state changes anywhere else
	},

}

if (typeof exports === "object") {
	ReactMeteor = exports;
} else {
	ReactMeteor = {};
}

ReactMeteor.Mixin = ReactMeteorMixin;
ReactMeteor.REACT_METEOR_INJECT_CHECK = true; // whether or not we should look for injected meteor values for components
					   		 	  			  // override to look (ie just for the initial Render)

/** Adler32 Hash (taken from React source) **/

var MOD = 65521;

// This is a clean-room implementation of adler32 designed for detecting
// if markup is not what we expect it to be. It does not need to be
// cryptographically strong, only reasonable good at detecting if markup
// generated on the server is different than that on the client.
function adler32(data) {
  var a = 1;
  var b = 0;
  for (var i = 0; i < data.length; i++) {
    a = (a + data.charCodeAt(i)) % MOD;
    b = (b + a) % MOD;
  }
  return a | (b << 16);
}

/** ** ** ** ** ** ** **/

var meteorReactChecksum = function (component) {
	var data = {};
	data._COMPONENT = component.constructor.displayName;

	if (component.props && component.constructor.meteorPropDeps) {
		_.each(component.constructor.meteorPropDeps, function (prop) {
			data['prop'+prop] = component.props[prop];
		});
	}

	if (component.state && component.constructor.meteorStateDeps) {
		_.each(component.constructor.meteorStateDeps, function (prop) {
			data['state'+prop] = component.state[prop];
		});
	}

	data = JSON.stringify(data);
	return adler32(data);
}

/** ** ** ** ** ** ** **/

// _meteor_awakenState
// Takes objects in state which have a collection transform applied, re runs that transformation on the client side
// Iterates through arrays to find but not object keys past the first level when we initially get our injected state (see main code above)

var _meteor_awakenState = function (data) {
	if (!data) {
		return null;
	} else if (data instanceof Array) {
		return _.map(data, function (_data) { return _meteor_awakenState(_data); });
	} else if (typeof data === 'object') {
		data._INJECTED = true; // need to flag it up so that when we reload on the client we see that this object is different so that we trigger meteorSubscribe changes if necessary based on prop changes
							   // but there is an inefficiency here - because in terms of the render, we don't really need to re render based on an identical object which only has a changed data origin (ie server vs client)

		if (data.__TRANSFORM__) return AllCollections[data.__TRANSFORM__]._transform(data);
	}
		
	return data;
}