Package.describe({
  name: 'howcloud:react-deps',
  summary: "React mixin for meteor dependency tracker based rendering/rerendering of components"
});

Package.on_use(function(api) {
	api.use('gadicohen:inject-initial');
	api.use('underscore');

	api.add_files("./ReactMeteor.js", ["server", "client"]);
	api.export("ReactMeteor", ["server", "client"]);
});